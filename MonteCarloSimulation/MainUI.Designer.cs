﻿namespace MonteCarloSimulation
{
    partial class MainUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainUI));
            this.listBoxResults = new System.Windows.Forms.ListBox();
            this.buttonRun = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonExport = new System.Windows.Forms.Button();
            this.historyListBox = new System.Windows.Forms.ListBox();
            this.historyLabel = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.buttonInformation = new System.Windows.Forms.Button();
            this.checkBoxSingleRun = new System.Windows.Forms.CheckBox();
            this.comboBoxConfidence = new System.Windows.Forms.ComboBox();
            this.comboBoxReplications = new System.Windows.Forms.ComboBox();
            this.comboBoxDot = new System.Windows.Forms.ComboBox();
            this.buttonAnimate = new System.Windows.Forms.Button();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.buttonExportValues = new System.Windows.Forms.Button();
            this.buttonExportTracking = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxResults
            // 
            this.listBoxResults.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.listBoxResults.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBoxResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxResults.ForeColor = System.Drawing.Color.White;
            this.listBoxResults.FormattingEnabled = true;
            this.listBoxResults.ItemHeight = 20;
            this.listBoxResults.Location = new System.Drawing.Point(16, 36);
            this.listBoxResults.Name = "listBoxResults";
            this.listBoxResults.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxResults.Size = new System.Drawing.Size(211, 420);
            this.listBoxResults.TabIndex = 16;
            this.listBoxResults.TabStop = false;
            // 
            // buttonRun
            // 
            this.buttonRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRun.Location = new System.Drawing.Point(861, 186);
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(150, 40);
            this.buttonRun.TabIndex = 33;
            this.buttonRun.Text = "Run";
            this.buttonRun.UseVisualStyleBackColor = true;
            this.buttonRun.Click += new System.EventHandler(this.bottonRun_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 24);
            this.label2.TabIndex = 34;
            this.label2.Text = "Means :";
            // 
            // buttonExport
            // 
            this.buttonExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExport.Location = new System.Drawing.Point(861, 232);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(150, 40);
            this.buttonExport.TabIndex = 36;
            this.buttonExport.Text = "Export History .txt";
            this.buttonExport.UseVisualStyleBackColor = true;
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // historyListBox
            // 
            this.historyListBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.historyListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.historyListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.historyListBox.ForeColor = System.Drawing.Color.White;
            this.historyListBox.FormattingEnabled = true;
            this.historyListBox.ItemHeight = 20;
            this.historyListBox.Location = new System.Drawing.Point(280, 36);
            this.historyListBox.Name = "historyListBox";
            this.historyListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.historyListBox.Size = new System.Drawing.Size(558, 420);
            this.historyListBox.TabIndex = 37;
            this.historyListBox.TabStop = false;
            // 
            // historyLabel
            // 
            this.historyLabel.AutoSize = true;
            this.historyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.historyLabel.Location = new System.Drawing.Point(276, 9);
            this.historyLabel.Name = "historyLabel";
            this.historyLabel.Size = new System.Drawing.Size(77, 24);
            this.historyLabel.TabIndex = 38;
            this.historyLabel.Text = "History :";
            // 
            // toolTip
            // 
            this.toolTip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.toolTip.ForeColor = System.Drawing.Color.White;
            // 
            // buttonInformation
            // 
            this.buttonInformation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInformation.Location = new System.Drawing.Point(861, 416);
            this.buttonInformation.Name = "buttonInformation";
            this.buttonInformation.Size = new System.Drawing.Size(150, 40);
            this.buttonInformation.TabIndex = 41;
            this.buttonInformation.Text = "Read Me";
            this.toolTip.SetToolTip(this.buttonInformation, "Read Me");
            this.buttonInformation.UseVisualStyleBackColor = true;
            this.buttonInformation.Click += new System.EventHandler(this.buttonInformation_Click);
            // 
            // checkBoxSingleRun
            // 
            this.checkBoxSingleRun.AutoSize = true;
            this.checkBoxSingleRun.Location = new System.Drawing.Point(861, 140);
            this.checkBoxSingleRun.Name = "checkBoxSingleRun";
            this.checkBoxSingleRun.Size = new System.Drawing.Size(151, 24);
            this.checkBoxSingleRun.TabIndex = 53;
            this.checkBoxSingleRun.Text = "Record Each Dot";
            this.toolTip.SetToolTip(this.checkBoxSingleRun, "Determines if we record each estimation of pi foreach nDots. Not efficient. Read " +
        "the Read me!");
            this.checkBoxSingleRun.UseVisualStyleBackColor = true;
            // 
            // comboBoxConfidence
            // 
            this.comboBoxConfidence.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.comboBoxConfidence.ForeColor = System.Drawing.Color.White;
            this.comboBoxConfidence.FormatString = "0.00";
            this.comboBoxConfidence.FormattingEnabled = true;
            this.comboBoxConfidence.Location = new System.Drawing.Point(861, 106);
            this.comboBoxConfidence.Name = "comboBoxConfidence";
            this.comboBoxConfidence.Size = new System.Drawing.Size(150, 28);
            this.comboBoxConfidence.TabIndex = 52;
            this.toolTip.SetToolTip(this.comboBoxConfidence, "Confidence Level");
            // 
            // comboBoxReplications
            // 
            this.comboBoxReplications.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.comboBoxReplications.ForeColor = System.Drawing.Color.White;
            this.comboBoxReplications.FormattingEnabled = true;
            this.comboBoxReplications.Location = new System.Drawing.Point(861, 72);
            this.comboBoxReplications.Name = "comboBoxReplications";
            this.comboBoxReplications.Size = new System.Drawing.Size(150, 28);
            this.comboBoxReplications.TabIndex = 50;
            this.toolTip.SetToolTip(this.comboBoxReplications, "Number of Replications");
            // 
            // comboBoxDot
            // 
            this.comboBoxDot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.comboBoxDot.ForeColor = System.Drawing.Color.White;
            this.comboBoxDot.FormattingEnabled = true;
            this.comboBoxDot.Location = new System.Drawing.Point(861, 36);
            this.comboBoxDot.Name = "comboBoxDot";
            this.comboBoxDot.Size = new System.Drawing.Size(150, 28);
            this.comboBoxDot.TabIndex = 47;
            this.toolTip.SetToolTip(this.comboBoxDot, "Number of Dots");
            // 
            // buttonAnimate
            // 
            this.buttonAnimate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAnimate.Location = new System.Drawing.Point(861, 370);
            this.buttonAnimate.Name = "buttonAnimate";
            this.buttonAnimate.Size = new System.Drawing.Size(150, 40);
            this.buttonAnimate.TabIndex = 40;
            this.buttonAnimate.Text = "Visualize";
            this.buttonAnimate.UseVisualStyleBackColor = true;
            this.buttonAnimate.Click += new System.EventHandler(this.buttonAnimate_Click);
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // buttonExportValues
            // 
            this.buttonExportValues.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExportValues.Location = new System.Drawing.Point(861, 278);
            this.buttonExportValues.Name = "buttonExportValues";
            this.buttonExportValues.Size = new System.Drawing.Size(150, 40);
            this.buttonExportValues.TabIndex = 44;
            this.buttonExportValues.Text = "Export .csv";
            this.buttonExportValues.UseVisualStyleBackColor = true;
            this.buttonExportValues.Click += new System.EventHandler(this.buttonExportValues_Click);
            // 
            // buttonExportTracking
            // 
            this.buttonExportTracking.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExportTracking.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExportTracking.Location = new System.Drawing.Point(861, 324);
            this.buttonExportTracking.Name = "buttonExportTracking";
            this.buttonExportTracking.Size = new System.Drawing.Size(150, 40);
            this.buttonExportTracking.TabIndex = 46;
            this.buttonExportTracking.Text = "Export Detailed .csv";
            this.buttonExportTracking.UseVisualStyleBackColor = true;
            this.buttonExportTracking.Click += new System.EventHandler(this.buttonExportTracking_Click);
            // 
            // MainUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.ClientSize = new System.Drawing.Size(1024, 469);
            this.Controls.Add(this.comboBoxConfidence);
            this.Controls.Add(this.comboBoxReplications);
            this.Controls.Add(this.comboBoxDot);
            this.Controls.Add(this.checkBoxSingleRun);
            this.Controls.Add(this.buttonExportTracking);
            this.Controls.Add(this.buttonExportValues);
            this.Controls.Add(this.buttonInformation);
            this.Controls.Add(this.buttonAnimate);
            this.Controls.Add(this.historyListBox);
            this.Controls.Add(this.historyLabel);
            this.Controls.Add(this.buttonExport);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonRun);
            this.Controls.Add(this.listBoxResults);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "MainUI";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Monte Carlo Simulation";
            this.Load += new System.EventHandler(this.MainUI_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxResults;
        private System.Windows.Forms.Button buttonRun;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonExport;
        private System.Windows.Forms.ListBox historyListBox;
        private System.Windows.Forms.Label historyLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button buttonAnimate;
        private System.Windows.Forms.Button buttonInformation;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.Button buttonExportValues;
        private System.Windows.Forms.Button buttonExportTracking;
        private System.Windows.Forms.CheckBox checkBoxSingleRun;
        private System.Windows.Forms.ComboBox comboBoxConfidence;
        private System.Windows.Forms.ComboBox comboBoxReplications;
        private System.Windows.Forms.ComboBox comboBoxDot;
    }
}