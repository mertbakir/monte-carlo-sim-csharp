## SD vs SEM

While the mean and standard deviation are descriptive statistics, the mean and standard error describes bounds for a random sampling process. This difference changes the meaning of what is being reported: a description of variation in measurements vs a statement of uncertainty around the estimate of the mean.

In other words standard error shows how close your sample mean is to the population mean. Standard deviation shows how much individuals within the same sample differ from the sample mean. This also means that standard error should decrease if the sample size increases, as the estimate of the population mean improves. Standard deviation will not be affected by sample size.


[Gregory Verleysen, ResearchGate](https://www.researchgate.net/post/Should_I_use_the_standard_deviation_or_the_standard_error_of_the_mean)


The SD indicates the dispersion of individual data values around their mean, and should be given any time we report data. The SE is an index of the variability of the means that would be expected if the study were exactly replicated a large number of times. By itself,this measure doesn’t convey much useful information. Its main function is to help construct 95% and 99% CIs, which can supplement statistical significance testing and indicate the range within which the true mean or difference between means maybe found.

[Maintaning Standards: Differences between the Standard Deviation and Standard Error, and When to Use Each](http://ww1.cpa-apc.org/Publications/Archives/PDF/1996/Oct/strein2.pdf)


-----

The Standard Deviation of the means is called The Standard Error.

Standard deviaton, tells us _how the data is distributed around the mean._
Standard error, tells us _how the mean is distributed._ 
Confidence Intervals are related to standard errors. Since we are trying to interpret the confidence interval of the average value. We are considering how averages (means) are distributed. 

## Skewness & Kurtosis

What's the acceptable range of skewness and kurtosis ?

-2 +2 are usually fine.

(Trochim & Donnelly, 2006; Field, 2000 & 2009; Gravetter & Wallnau, 2014) 
(George & Mallery, 2010) SPSS for Windows Step by Step: A Simple Guide and Reference, 17.0 update (10a ed.) 


> It depends on mainly the sample size. Most software packages that compute the skewness and kurtosis, also compute their standard error.
> Both 
> S = skewness/SE(skewness)
> and
> K = kurtosis/SE(kurtosis)
> are, under the null hypothesis of normality, roughly standard normal distributed.
> Thus, when |S| > 1.96 the skewness is significantly (alpha=5%) different from zero; the same for |K| > 1.96 and the kurtosis.

[Source: Casper J Albers](https://www.researchgate.net/post/What_is_the_acceptable_range_of_skewness_and_kurtosis_for_normal_distribution_of_data)


How to compute SE of skewsness and SE of kurtosis ?

Roughly,

> SE(skewness) = SQRT(6/N) 
> SE(kurtosis) = SQRT(24/N)